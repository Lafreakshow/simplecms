tinymce.init({
	selector: "textarea",
	plugins: 'link image code',
	relative_urls: false, //Relative URL in (Aboslute) Umwandeln
	image_list: '../js/image_list.php', //Json data wird von PHP generiert; Muster: [{title: "Blaue Berge.jpg", value: "../res/media/img/Blaue Berge.jpg"}]
	//width: 800,
	height: 400
});