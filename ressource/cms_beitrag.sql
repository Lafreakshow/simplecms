-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 01. Mai 2014 um 17:22
-- Server Version: 5.5.32
-- PHP-Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `cms`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cms_beitrag`
--

CREATE TABLE IF NOT EXISTS `cms_beitrag` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `written` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(40) COLLATE utf8_bin NOT NULL,
  `head` varchar(100) COLLATE utf8_bin NOT NULL,
  `text` longtext COLLATE utf8_bin NOT NULL,
  `kat_ID` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=19 ;

--
-- Daten für Tabelle `cms_beitrag`
--

INSERT INTO `cms_beitrag` (`ID`, `written`, `user`, `head`, `text`, `kat_ID`, `visible`) VALUES
(10, '2014-03-19 09:15:44', 'User_B', 'Titel_B', 'Post_B', 2, 1),
(9, '2014-03-19 09:15:26', 'User_A', 'Titel_A', 'Post_A', 1, 0),
(12, '2014-03-28 12:17:33', 'Lafreakshow', 'Bilder', '<p><img src="/CMS_2/res/media/img/Wasserlilien.jpg" alt="Wasserlilien.jpg" width="100" /><img src="/CMS_2/res/media/img/Winter.jpg" alt="Winter.jpg" width="344" /></p>', 2, 1),
(13, '2014-03-28 12:18:02', 'Lafreakshowh', 'ÜÜÜÜh', '<p>A&Uuml;&Ouml;</p>\r\n<p>g</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 0, 0),
(15, '2014-04-01 12:06:57', 'Lafreakshow', 'ÜÜÜÜ', '<p>A&Uuml;&Ouml;</p>', 0, 1),
(16, '2014-04-01 12:12:52', 'Lafreakshow', 'fgh', '<p>gfh</p>', 0, 0),
(17, '2014-04-01 12:13:09', 'Lafreakshow', 'fgh', '<p>gfh</p>', 0, 0),
(18, '2014-04-01 12:13:23', 'Lafreakshow', 'df', '<p>dfdsf</p>', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
