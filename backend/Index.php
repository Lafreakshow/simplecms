<html>
	<?php

	function ActivePageLink($name) {
		if (isset($_GET['a'])) {
			if ($_GET['a'] == $name) {
				echo 'button_a';
			}
		} elseif ($name == 'Start') {
			echo 'button_a';
		}
	}

	function ActiveSubLink($name) {
		if (isset($_GET['sub'])) {
			if ($_GET['sub'] == $name) {
				echo 'button_a';
			}
		}
	}
	?>
	<head>
		<title>CMS -- Backend</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../css/general.css">
		<link rel="stylesheet" href="../css/backend_general.css">
		<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="../js/editorInit.js"></script>
	</head>
	<body>
		<header>
			<h1>Backend</h1>
		</header>
		<div id="wrapper">
			<article>
				<?php
				if (isset($_GET['a'])) {
					if (isset($_GET['sub'])) {
						include $_GET['a'] . "/" . $_GET['sub'] . ".php";
					} else {
						if (file_exists($_GET['a'] . '/home.php')) {
							include $_GET['a'] . '/home.php';
						} else {
							include 'home.php';
						}
					}
				} else {
					include 'home.php';
				}
				?>
			</article>
			<nav>
				NAVIGATION
				<ul id="NavLinkList">
					<li><a href="?a=Start" class="button <?php ActivePageLink('Start') ?>">Startseite</a></li>
					<li><a href="?a=PostControl&sub=Show" class="button <?php ActivePageLink('PostControl') ?>">Beiträge</a></li>
					<?php
					if (isset($_GET['a']) && $_GET['a'] == "PostControl") {
						?>
						<ul id="SubNavLinkList">
							<li><a class="button <?php ActiveSubLink('Create') ?>" href="?a=PostControl&sub=Create">Erstellen</a></li>
							<li><a class="button <?php ActiveSubLink('Search') ?>" href="?a=PostControl&sub=Search">Suchen</a></li>
						</ul>
						<?php
					}
					?>
					<li><a href="?a=NavControl&sub=Show" class="button <?php ActivePageLink('NavControl') ?>">Navigation</a></li>
					<?php
					if (isset($_GET['a']) && $_GET['a'] == "NavControl") {
						?>
						<ul id="SubNavLinkList">
							<li><a class="button <?php ActiveSubLink('Create') ?>" href="?a=NavControl&sub=Create">Erstellen</a></li>
							<li><a class="button <?php ActiveSubLink('Search') ?>" href="?a=NavControl&sub=Search">Suchen</a></li>
						</ul>
						<?php
					}
					?>
					<li><a href="?a=UserControl" class="button <?php ActivePageLink('UserControl') ?>">Benutzer</a></li>
					<?php
					if (isset($_GET['a']) && $_GET['a'] == "UserControl") {
						?>
						<ul id="SubNavLinkList">
							<li><a class="button <?php ActiveSubLink('Create') ?>" href="?a=UserControl&sub=Create">Erstellen</a></li>
							<li><a class="button <?php ActiveSubLink('Search') ?>" href="?a=UserControl&sub=Search">Suchen</a></li>
						</ul>
						<?php
					}
					?>
					<li><a href="?a=ImageControl&sub=show" class="button <?php ActivePageLink('ImageControl') ?>">Bilder</a></li>
					<?php
					if (isset($_GET['a']) && $_GET['a'] == "ImageControl") {
						?>
						<ul id="SubNavLinkList">
							<li><a class="button <?php ActiveSubLink('Upload') ?>" href="?a=ImageControl&sub=Upload">Hochladen</a></li>
						</ul>
						<?php
					}
					?>
					<li><a href = "../" class = "button" target = "_blank">Frontend</a></li>
				</ul>
			</nav>
		</div>

		<footer>
			<h3>Impressum</h3> Verantwortlicher: Max Mustermann -- Musterstraße 1 -- 12345 Musterstadt
		</footer>
	</body>
</html>