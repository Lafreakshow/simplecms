<h1>Beiträge - Ändern</h1>
<?php
if (isset($_POST['postEditSubmit'])) {
	$id = $_GET['id'];
	$title = $_POST['postEditTitle'];
	$kat = $_POST['postEditKat'];
	$user = $_POST['postEditAuthor'];
	$text = $_POST['postEditText'];
	$visible = (isset($_POST['postEditVisible']) ? 1 : 0);

	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

	$sql = "UPDATE cms_beitrag SET user = '$user', head = '$title', text = '$text', kat_ID = '$kat', visible = '$visible' WHERE ID = $id;";
	if ($dbh->query($sql)) {
		?>
		<p>
			Eintrag Wurde Geändert<br>
			<a href="?a=PostControl&sub=Show">Zurück zur Übersicht</a>
		</p>
		<?php
	} else {
		echo "Fehler";
	}
} elseif (isset($_GET['id'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$sql = "SELECT * FROM cms_beitrag WHERE ID = $_GET[id]";
	$row = $dbh->query($sql)->fetch();
	?>
	<form id="BackendEditForm" method="post">
		<table>
			<tr>
				<td><label for="postEditTitle">Title: </label></td>
				<td><input class="button" name="postEditTitle" id="postEditTitle" type="text" size="50" required value="<?php echo $row["head"]; ?>"></td>
			</tr>
			<tr>
				<td><label for="postEditAuthor">Autor: </label></td>
				<td><input class="button" name="postEditAuthor" id="postEditAuthor" type="text" size="50" required value="<?php echo $row["user"]; ?>"></td>
			</tr>
			<tr>
				<td><label for="postEditKat">Kategorie ID: </label></td>
				<td><input class="button" name="postEditKat" id="postEditKat" type="text" size="50" required value="<?php echo $row["kat_ID"]; ?>"></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name="postEditText"><?php echo $row["text"]; ?></textarea></td>
			</tr>
			<tr>
				<td><label for="postEditVisible">Sichtbar: </label></td>
				<td><input class="button" name="postEditVisible" id="postEditVisible" type="checkbox" <?php echo ($row['visible'] ? "checked" : "") ?>></td>
			</tr>
			<tr>
				<td colspan="2"><input class="button" type="submit" name="postEditSubmit" value="Eintragen"></td>
			</tr>
		</table>
	</form>
	<?php
}