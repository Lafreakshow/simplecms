<h1>Beiträge</h1>
<?php
if (isset($_POST['postVisible'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	if ($_POST['postVisible'] == "Aktivieren") {
		$sql = "UPDATE cms_beitrag SET visible = '1' WHERE ID = " . $_POST['postVisibleID'];
	} else {
		$sql = "UPDATE cms_beitrag SET visible = '0' WHERE ID = " . $_POST['postVisibleID'];
	}
	$dbh->query($sql);
}
?>
<?php
include '../php/dbInf.php';
$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
if (isset($_GET['order'])) {
	$statement = "SELECT * FROM cms_beitrag ORDER BY " . $_GET['order'] . " ASC LIMIT 0, 30";
} else {
	$statement = "SELECT * FROM cms_beitrag LIMIT 0, 30";
}
?>
<table id="BackendTable">
	<tr>
		<th><a href="?a=PostControl&sub=Show&order=kat_ID">Kat.</a></th>
		<th><a href="?a=PostControl&sub=Show&order=head">Title</a></th>
		<th><a href="?a=PostControl&sub=Show&order=user">Autor</a></th>
		<th><a href="?a=PostControl&sub=Show&order=written">Datum</a></th>
		<th><a href="?a=PostControl&sub=Show&order=visible">Sichtbar</a></th>
	</tr>
	<?php
	foreach ($stmt = $dbh->query($statement) as $row) {
		?>
		<tr id="<?php echo $row["ID"]; ?>">
			<td class="short"><?php echo $row['kat_ID'] ?></td>
			<td><?php echo $row['head'] ?></td>
			<td><?php echo $row['user'] ?></td>
			<td><?php echo strftime("%m-%d-%Y", strtotime($row['written'])) . "<br>" . strftime("%H:%M:%S", strtotime($row['written'])) ?></td>
			<td><?php
				if ($row['visible']) {
					echo 'Ja';
				} else {
					echo 'Nein';
				}
				?></td>
			<td class="form">
				<form method="post" action="#<?php echo $row['ID'] ?>">
					<input class="button" type="submit" name="postVisible" value="<?php
					if ($row['visible']) {
						echo "Deaktivieren";
					} else {
						echo 'Aktivieren';
					}
					?>">
					<input type="hidden" value="<?php echo $row['ID'] ?>" name="postVisibleID">
				</form>
				<a href="?a=PostControl&sub=Edit&id=<?php echo $row['ID'] ?>" class="button">Ändern</a>
				<a href="?a=PostControl&sub=Delete&id=<?php echo $row['ID'] ?>" class="button">Löschen</a>
				<a target="_blank" href="../?post=<?php echo $row['ID'] ?>" class="button">Frontend</a>
			</td>
		</tr>
		<?php
	}
	?>

</table>