<h1>Beiträge - Löschen</h1>
<?php
if (isset($_POST['DelNo'])) {
	?>
	<p>Eintrag Wird Behalten</p>
	<a href="?a=PostControl&sub=Show">Zurück zur Übersicht</a>
	<?php
} elseif (isset($_POST['DelYes'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

	$sql = "DELETE FROM cms_beitrag WHERE id=" . $_POST['DelID'];
	$dbh->query($sql);
	?>
	<p>Eintrag Gelöscht</p>
	<a href="?a=PostControl&sub=Show">Zurück zur Übersicht</a>
	<?php
} elseif (isset($_GET['id'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

	$statement = "SELECT * FROM cms_beitrag WHERE ID = $_GET[id]";
	?>
	<table id="BackendTable">
		<tr>
			<th><a href="?a=PostControl&sub=Show&order=kat_ID">Kat.</a></th>
			<th><a href="?a=PostControl&sub=Show&order=head">Title</a></th>
			<th><a href="?a=PostControl&sub=Show&order=user">Autor</a></th>
			<th><a href="?a=PostControl&sub=Show&order=written">Datum</a></th>
			<th><a href="?a=PostControl&sub=Show&order=visible">Sichtbar</a></th>
		</tr>
		<?php
		foreach ($stmt = $dbh->query($statement) as $row) {
			?>
			<tr id="<?php echo $row["ID"]; ?>">
				<td class="short"><?php echo $row['kat_ID'] ?></td>
				<td><?php echo $row['head'] ?></td>
				<td><?php echo $row['user'] ?></td>
				<td><?php echo strftime("%m-%d-%Y", strtotime($row['written'])) . "<br>" . strftime("%H:%M:%S", strtotime($row['written'])) ?></td>
				<td><?php
					if ($row['visible']) {
						echo 'Ja';
					} else {
						echo 'Nein';
					}
					?></td>
			</tr>
			<?php
		}
		?>
	</table>
	<form method="post">
		<input class="button" type="submit" name="DelYes" value="Ja, Wirklich Löschen">
		<input class="button" type="submit" name="DelNo" value="Nein, Eintrag Behalten">
		<input type="hidden" name="DelID" value="<?php echo $_GET['id'] ?>">
	</form>
	<?php
} else {
	echo "<center>Keine ID Angegeben</center>";
}
?>
