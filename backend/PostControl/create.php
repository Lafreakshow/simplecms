<h1>Beiträge - Neu</h1>
<?php
if (isset($_POST['postNewSubmit'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$title = $_POST['postNewTitle'];
	$kat = $_POST['postNewKat'];
	$user = $_POST['postNewAuthor'];
	$text = $_POST['postNewText'];
	$visible = (isset($_POST['postNewvisible']) ? 1 : 0);

	$sql = "INSERT INTO cms_beitrag (ID, head, kat_ID, user, text, visible) VALUES (NULL, '$title', '$kat', '$user', '$text', '$visible');";
	if ($dbh->query($sql)) {
		?>
		Erfolgreich Engetragen<br>
		<a href="?a=PostControl&sub=Show">Zurück zur Übersicht</a><br>
		<a href="?a=PostControl&sub=Create">Weitere Kategorie anlegen</a>
		<?php
	} else {
		echo "<center>Fehler</center>";
	}
} else {
	?>
	<form id="BackendEditForm" method="post">
		<table>
			<tr>
				<td><label for="postNewTitle">Title: </label></td>
				<td><input class="button" name="postNewTitle" id="postNewTitle" type="text" size="50" required></td>
			</tr>
			<tr>
				<td><label for="postNewAuthor">Autor: </label></td>
				<td><input class="button" name="postNewAuthor" id="postNewAuthor" type="text" size="50" required></td>
			</tr>
			<tr>
				<td><label for="postNewKat">Kategorie ID: </label></td>
				<td><input class="button" name="postNewKat" id="postNewKat" type="text" value="" size="50" required></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name="postNewText"></textarea></td>
			</tr>
			<tr>
				<td><label for="postNewVisible">Sichtbar: </label></td>
				<td><input class="button" name="postNewVisible" id="postNewVisible" type="checkbox"></td>
			</tr>
			<tr>
				<td colspan="2"><input class="button" type="submit" name="postNewSubmit" value="Eintragen"></td>
			</tr>
		</table>
	</form>
	<?php
}