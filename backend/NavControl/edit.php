<h1>Navigation - Ändern</h1>
<?php
if (isset($_POST['navEditSubmit'])) {
	$id = $_POST['navEditID'];
	$title = $_POST['navEditTitle'];
	$kat = $_POST['navEditKat'];
	$pos = $_POST['navEditPos'];
	$visible = (isset($_POST['navEditVisible']) ? 1 : 0);

	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

	$sql = "UPDATE navigation SET title = '$title', kat_ID = '$kat', pos = '$pos', visible = '$visible' WHERE ID = $id;";
	$dbh->query($sql);
	?>
	<p>
		Eintrag Wurde Geändert<br>
		<a href="?a=NavControl&sub=Show">Zurück zur Übersicht</a>
	</p>

	<?php
} elseif (isset($_GET['id'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$sql = "SELECT * FROM navigation WHERE ID = " . $_GET['id'];
	$row = $dbh->query($sql)->fetch();
	?>
	<form id="BackendEditForm" method="post">
		<table>
			<tr>
				<td><label for="navEditID">ID: </label></td>
				<td><input class="button" name="navEditID" id="navEditID" type="number" readonly value="<?php echo $row['ID']; ?>" required></td>
			</tr>
			<tr>
				<td><label for="navEditTitle">Title: </label></td>
				<td><input class="button" name="navEditTitle" id="navEditTitle" type="text" value="<?php echo $row['title']; ?>" size="50" required></td>
			</tr>
			<tr>
				<td><label for="navEditKat">Kategorie ID: </label></td>
				<td><input class="button" name="navEditKat" id="navEditKat" type="text" value="<?php echo $row['kat_ID']; ?>" size="50" required></td>
			</tr>
			<tr>
				<td><label for="navEditPos">Position: </label></td>
				<td><input class="button" name="navEditPos" id="navEditPos" type="number" value="<?php echo $row['pos']; ?>" required></td>
			</tr>
			<tr>
				<td><label for="navEditVisible">Sichtbar: </label></td>
				<td><input class="button" name="navEditVisible" id="navEditVisible" type="checkbox" <?php echo $row['visible'] ? 'checked' : ''; ?>></td>
			</tr>
			<tr>
				<td colspan="2"><input class="button" type="submit" name="navEditSubmit" value="Ändern"></td>
			</tr>
		</table>
	</form>
	<?php
} else {
	echo "<center>Keine ID angegeben</center>";
}
?>