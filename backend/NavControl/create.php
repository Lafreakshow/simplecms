<h1>Navigation - Neu</h1>
<?php
if (isset($_POST['navNewSubmit'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$title = $_POST['navNewTitle'];
	$kat = $_POST['navNewKat'];
	$pos = $_POST['navNewPos'];
	$visible = (isset($_POST['navNewvisible']) ? 1 : 0);

	$sql = "INSERT INTO navigation (ID, title, kat_ID, pos, visible) VALUES (NULL, '$title', '$kat', '$pos', '$visible');";
	if ($dbh->query($sql)) {
		?>
		Erfolgreich Engetragen<br>
		<a href="?a=NavControl&sub=Show">Zurück zur Übersicht</a><br>
		<a href="?a=NavControl&sub=Create">Weitere Kategorie anlegen</a>
		<?php
	} else {
		echo "<center>Fehler</center>";
	}
} else {
	?>
	<form id="BackendEditForm" method="post">
		<table>
			<tr>
				<td><label for="navEditTitle">Title: </label></td>
				<td><input class="button" name="navNewTitle" id="navEditTitle" type="text" size="50" required></td>
			</tr>
			<tr>
				<td><label for="navEditKat">Kategorie ID: </label></td>
				<td><input class="button" name="navNewKat" id="navEditKat" type="text" value="" size="50" required></td>
			</tr>
			<tr>
				<td><label for="navEditPos">Position: </label></td>
				<td><input class="button" name="navNewPos" id="navEditPos" type="number"required></td>
			</tr>
			<tr>
				<td><label for="navEditVisible">Sichtbar: </label></td>
				<td><input class="button" name="navNewVisible" id="navEditVisible" type="checkbox"></td>
			</tr>
			<tr>
				<td colspan="2"><input class="button" type="submit" name="navNewSubmit" value="Eintragen"></td>
			</tr>
		</table>
	</form>
	<?php
}