<h1>Navigation - Suche</h1>
<?php
if (!isset($_POST['navSearchSubmit']) && !isset($_GET['q'])) {
	?>
	<form id = "BackendEditForm" method = "post">
		<table>
			<tr>
				<td><label for = "navSearchTitle">Title: </label></td>
				<td><input class = "button" name = "navSearchTitle" id = "navSearchTitle" type = "text" size = "50" required></td>
			</tr>
			<tr>
				<td colspan = "2"><input class = "button" type = "submit" name = "navSearchSubmit" value = "Suchen"></td>
			</tr>
		</table>
	</form>
	<?php
} else {
	if (isset($_POST['navVisible'])) {
		include '../php/dbInf.php';
		$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		if ($_POST['navVisible'] == "Aktivieren") {
			$sql = "UPDATE navigation SET visible = '1' WHERE ID = " . $_POST['navVisibleID'];
		} else {
			$sql = "UPDATE navigation SET visible = '0' WHERE ID = " . $_POST['navVisibleID'];
		}
		$dbh->query($sql);
	}

	if (isset($_GET['q'])) {
		$q = $_GET['q'];
	} elseif (isset($_POST['navSearchTitle'])) {
		$q = $_POST['navSearchTitle'];
	}
	?>
	<h1>Navigation</h1>
	<br>
	<?php
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	if (isset($_GET['order'])) {
		$statement = "SELECT * FROM navigation WHERE title LIKE '%$q%' ORDER BY " . $_GET['order'] . " ASC LIMIT 0, 30 ";
	} else {
		$statement = "SELECT * FROM navigation WHERE title LIKE '%$q%' LIMIT 0, 30";
	}
	?>
	<table id="BackendTable">
		<tr>
			<th><a href="?a=NavControl&sub=Show&order=title">Title</a></th>
			<th><a href="?a=NavControl&sub=Show&order=kat_ID">Kategorie</a></th>
			<th><a href="?a=NavControl&sub=Show&order=pos">Position</a></th>
			<th><a href="?a=NavControl&sub=Show&order=visible">Sichtbar</a></th>
		</tr>
		<?php
		foreach ($stmt = $dbh->query($statement) as $row) {
			?>
			<tr>
				<td><?php echo $row['title'] ?></td>
				<td><?php echo $row['kat_ID'] ?></td>
				<td><?php echo $row['pos'] ?></td>
				<td><?php
					if ($row['visible']) {
						echo 'Ja';
					} else {
						echo 'Nein';
					}
					?></td>
				<td class="form">
					<form method="post" action="?a=NavControl&sub=Search&q=<?php echo $q ?>">
						<input class="button" type="submit" name="navVisible" value="<?php
						if ($row['visible']) {
							echo "Deaktivieren";
						} else {
							echo 'Aktivieren';
						}
						?>">
						<input type="hidden" value="<?php echo $row['ID'] ?>" name="navVisibleID">
					</form>
				</td>
				<td class="form"><a href="?a=NavControl&sub=Edit&id=<?php echo $row['ID'] ?>" class="button">Ändern</a></td>
				<td class="form"><a href="?a=NavControl&sub=Delete&id=<?php echo $row['ID'] ?>" class="button">Löschen</a></td>
			</tr>
			<?php
		}
		?>
	</table>
	<?php
}