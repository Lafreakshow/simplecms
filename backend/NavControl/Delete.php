<h1>Navigation - Löschen</h1>
<?php
if (isset($_POST['DelNo'])) {
	?>
	<p>Eintrag Wird Behalten</p>
	<a href="?a=NavControl&sub=Show">Zurück zur Übersicht</a>
	<?php
} elseif (isset($_POST['DelYes'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

	$sql = "DELETE FROM navigation WHERE id=" . $_POST['DelID'];
	$dbh->query($sql);
	?>
	<p>Eintrag Gelöscht</p>
	<a href="?a=NavControl&sub=Show">Zurück zur Übersicht</a>
	<?php
} elseif (isset($_GET['id'])) {
	include '../php/dbInf.php';
	$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$sql = "SELECT * FROM navigation WHERE ID = " . $_GET['id'];
	$row = $dbh->query($sql)->fetch();
	?>
	<table id="BackendTable">
		<tr>
			<th><a href="?a=NavControl&sub=Show&order=title">Title</a></th>
			<th><a href="?a=NavControl&sub=Show&order=kat_ID">Kategorie</a></th>
			<th><a href="?a=NavControl&sub=Show&order=pos">Position</a></th>
			<th><a href="?a=NavControl&sub=Show&order=visible">Sichtbar</a></th>
		</tr>
		<tr>
			<td><?php echo $row['title']; ?></td>
			<td><?php echo $row['kat_ID']; ?></td>
			<td><?php echo $row['pos']; ?></td>
			<td><?php
				if ($row['visible']) {
					echo 'Ja';
				} else {
					echo 'Nein';
				}
				?></td>
		</tr>
	</table>
	<p>
		Disen Eintrag wirklich Löschen?<br>
		(Alle dieser Kategorie Zugeordneten Einträge werden nicht mehr Angezeigt)
	</p>
	<form method="post">
		<input class="button" type="submit" name="DelYes" value="Ja, Wirklich Löschen">
		<input class="button" type="submit" name="DelNo" value="Nein, Eintrag Behalten">
		<input type="hidden" name="DelID" value="<?php echo $_GET['id'] ?>">
	</form>

	<?php
} else {
	echo "<center>Keine ID Angegeben</center>";
}
?>