<form method="post" enctype="multipart/form-data">
	<input type="file" name="file">
	<input type="submit" name="submit" value="Hochladen">
</form>
<?php
if (isset($_POST['submit'])) {
	$filetype = getimagesize($_FILES['file']['tmp_name']);
	if ($filetype[2] != 0) {
		if ($_FILES['file']['size'] < 2024000) {
			move_uploaded_file($_FILES['file']['tmp_name'], "../media/img/" . $_FILES['file']['name']);
			echo "Bild Erfolgreich hochgeladen";
		} else {
			echo "Das bild darf nicht größer als 2mb sein";
		}
	} else {
		echo "nur datei im Gif/jpg Format erlaubt";
	}
}