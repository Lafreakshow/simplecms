<?php
$dirpath = '../media/img/';
?>
<table id="BackendTable">
	<tr><th>Image</th><th>Title</th><th>Type</th><th>Size</th></tr>
	<?php
	foreach (new DirectoryIterator($dirpath) as $file) {
		if (!$file->isDir() && !$file->isDot()) {
			$fileinf = explode(".", $file->getFilename());
			?>
			<tr>
				<td><img alt="<?php echo $file->getFilename() ?>" width="100" src="<?php echo $file->getPathname() ?>"></td>
				<td><?php echo $fileinf[0]; ?></td>
				<td><?php echo $fileinf[1]; ?></td>
				<td><?php echo round($file->getSize() / 1024, 2) ?>kb</td>
				<td>
					<a href="?a=ImageControl&sub=Delete&file=<?php echo $file->getFilename(); ?>" class="button">Löschen</a>
				</td>
			</tr>
			<?php
		}
	}
	?>
</table>