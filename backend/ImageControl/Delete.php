<h1>Bilder - Löschen</h1>
<?php
if (isset($_POST['DelFile'])) {
	if (isset($_POST['DelNo'])) {
		?>
		<p>Bild wir nicht Gelöscht</p>
		<a href="?a=ImageControl&sub=Show">Zurück zur Übersicht</a>
		<?php
	} elseif (isset($_POST['DelYes'])) {
		unlink("../media/img/" . $_POST['DelFile']);
		?>
		<p>Eintrag Gelöscht</p>
		<a href="?a=ImageControl&sub=Show">Zurück zur Übersicht</a>
		<?php
	}
} elseif (isset($_GET['file'])) {
	$file = $_GET['file'];
	?>
	<p>"<?php echo $file ?>" wirklich Löschen?</p>
	<form method="post">
		<input class="button" type="submit" name="DelYes" value="Ja, Wirklich Löschen">
		<input class="button" type="submit" name="DelNo" value="Nein, Eintrag Behalten">
		<input type="hidden" name="DelFile" value="<?php echo $file ?>">
	</form>
	<?php
} else {
	echo "<center>Fehler!</center>";
}
