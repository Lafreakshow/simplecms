<html>
	<?php

	//Funktion durch eigene Ersetzen!
	function get_snippet($str, $wordCount = 30) {
		return implode(
				'', array_slice(
						preg_split(
								'/([\s,\.;\?\!]+)/', $str, $wordCount * 2 + 1, PREG_SPLIT_DELIM_CAPTURE
						), 0, $wordCount * 2 - 1
				)
		);
	}
	?>
	<head>
		<title>CMS -- Frontend</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/general.css">
		<link rel="stylesheet" href="css/frontend.css">
	</head>
	<body>
		<header>
			<h1>Frontend</h1>
		</header>
		<div id="wrapper">
			<nav>
				<?php
				include 'php/dbInf.php';
				$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				$statement = "SELECT * FROM navigation ORDER BY pos ASC";
				foreach ($stmt = $dbh->query($statement) as $row) {
					if ($row['visible']) {
						?> <p><a class="button <?php
						if (isset($_GET['id'])) {
							if ($_GET['id'] == $row['kat_ID']) {
								echo 'button_a';
							}
						} elseif ($row['kat_ID'] == 0) {
							echo 'button_a';
						}
						?>" href="?id=<?php echo $row['kat_ID']; ?> "> <?php echo $row['title'] ?> </a></p> <?php
						}
					}
					?>
			</nav>
			<article>
				<?php
				if (isset($_GET['post'])) {
					include 'php/dbInf.php';
					$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					$sql = "SELECT * FROM cms_beitrag WHERE ID = $_GET[post] ORDER BY written DESC";
					foreach ($stmt = $dbh->query($sql) as $row) {
						if ($row['visible']) {
							?>
							<div class="post">
								<p class="title"><a href="?post=<?php echo $row['ID']; ?>"><?php echo $row['head']; ?></a></p>
								<div class="text"><?php echo $row['text']; ?></div>
								<p class="info">Geschrieben von <?php echo $row['user']; ?> -- <?php echo $row['written']; ?></p>
							</div>
							<?php
						} else {
							echo "<center>Beitrag Gesperrt</center>";
						}
					}
				} elseif (isset($_GET['id'])) {
					include 'php/dbInf.php';
					$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					$sql = "SELECT * FROM cms_beitrag WHERE kat_ID = $_GET[id] ORDER BY written DESC";
					foreach ($stmt = $dbh->query($sql) as $row) {
						if ($row['visible']) {
							?>
							<div class="post">
								<p class="title"><a href="?post=<?php echo $row['ID']; ?>"><?php echo $row['head']; ?></a></p>
								<div class="text"><?php echo get_snippet($row['text']); ?> <a class="cont" href="?post=<?php echo $row['ID']; ?>">Weiterlesen</a></div>
								<p class="info">Geschrieben von <?php echo $row['user']; ?> -- <?php echo $row['written']; ?></p>
							</div>
							<?php
						}
					}
				} else {
					include 'php/dbInf.php';
					$dbh = new PDO($db_host, $db_username, $db_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					$sql = "SELECT * FROM cms_beitrag WHERE kat_ID = 0 ORDER BY written DESC";
					foreach ($stmt = $dbh->query($sql) as $row) {
						if ($row['visible']) {
							?>
							<div class="post">
								<p class="title"><a href="?post=<?php echo $row['ID']; ?>"><?php echo $row['head']; ?></a></p>
								<div class="text"><?php echo get_snippet($row['text']); ?> <a class="cont" href="?post=<?php echo $row['ID']; ?>">Weiterlesen</a></div>
								<p class="info">Geschrieben von <?php echo $row['user']; ?> -- <?php echo $row['written']; ?></p>
							</div>
							<?php
						}
					}
				}
				?>
			</article>
		</div>

		<footer>
			<h3>Impressum</h3> Verantwortlicher: Max Mustermann -- Musterstraße 1 -- 12345 Musterstadt
		</footer>
	</body>
</html>